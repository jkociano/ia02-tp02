import sys
import itertools
import networkx as nx
import matplotlib.pyplot as plt
import fstrings

#pour passer en DIMACS : 
## faire le vocabulaire : voc = ("S1R" , "S1G" , "S1B" , ...)
## on fait un dico voc, qui associe des num à chaque item du voc

class Graph:
    sommets = 1
    arcs = []

    def __init__(self, nbSommets, arcs):
        self.sommets = nbSommets
        self.arcs = arcs
    
    def getSommets(self):
        return self.sommets
    
    def getArcs(self):
        return self.arcs

graf = Graph(5,[(1,2),(2,3),(2,4),(3,5),(4,5)])
colors = ["R" , "G" , "B"]

def createVocDico(graf : Graph, colors):
    voc = []
    dicoVoc = {}
    num = 1
    for i in range(graf.sommets):
        for j in range(len(colors)):
            voc = voc.append([f"S{i+1}{colors[j]}"])
            dicoVoc[f"S{i+1}{colors[j]}"] = num
            num = num + 1
    return voc 

def createClauses(voc):        
     


def graphToDIMACS(clauses):

